# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

variables:
  GIT_DEPTH: 1
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - build
  - release

include:
  - remote: https://raw.githubusercontent.com/dehesselle/sdkchecksum/master/.gitlab-ci/verify_sdk-template.yml

#-------------------------------------------------------- dehesselle/siril_macos

siril:
  stage: build
  parallel:
    matrix:
      - ARCHITECTURE: [ "arm64", "x86_64" ]
  rules:
    - if: $CI_PROJECT_NAMESPACE != "dehesselle"
      when: never
    - if: $ARCHITECTURE == "x86_64"
      variables:
        SDKROOT: /opt/sdks/MacOSX10.15.6.sdk
    - if: $ARCHITECTURE == "arm64"
      variables:
        SDKROOT: /opt/sdks/MacOSX11.3.sdk
  variables:
    SIRIL_BUILD: $CI_PIPELINE_IID
  tags:
    - macos
    - ${ARCHITECTURE}
  script:
    - !reference [ .verify_sdk, script ]
    - ./build_toolset.sh
    - ./build_siril.sh
  after_script:
    # For persistent runners: cleanup afterwards.
    - |
      VER_DIR=$(jhb/usr/bin/config get VER_DIR)
      rm -rf ${VER_DIR:?}
  artifacts:
    paths:
      - Siril*.dmg

#-------------------------------------------------------- free-astro/siril_macos

dependencies:build:
  stage: build
  parallel:
    matrix:
      - ARCHITECTURE: [ "arm64", "x86_64" ]
  rules:
    - if: $CI_PROJECT_NAMESPACE != "free-astro"
      when: never
    - if: $ARCHITECTURE == "arm64"
      variables:
        SDKROOT: /opt/sdks/MacOSX11.3.sdk
    - if: $ARCHITECTURE == "x86_64"
      when: manual
      allow_failure: true
      variables:
        SDKROOT: /opt/sdks/MacOSX10.15.6.sdk
  tags:
    - macos
    - ${ARCHITECTURE}
  script:
    - !reference [ .verify_sdk, script ]
    - ./build_toolset.sh
    # Artifact size needs to stay below 1 GiB for GitLab.
    - |
      jhb/usr/bin/jhb run rustup self uninstall -y || true
      jhb/usr/bin/archive remove_nonessentials
      jhb/usr/bin/archive create_dmg
  after_script:
    # For persistent runners: cleanup afterwards.
    - |
      VER_DIR=$(jhb/usr/bin/config get VER_DIR)
      rm -rf ${VER_DIR:?}
  artifacts:
    paths:
      - jhb*.dmg

dependencies:upload:
  stage: release
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG =~ /^r\d+/
  needs:
    - dependencies:build
  variables:
    PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/dependencies/${CI_COMMIT_TAG}"
  script:
    - |
      for ARTIFACT in jhb*.dmg; do
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${ARTIFACT} ${PACKAGE_REGISTRY_URL}/${ARTIFACT}
      done

dependencies:release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG =~ /^r\d+/
  needs:
    - dependencies:build
    - dependencies:upload
  variables:
    PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/dependencies/${CI_COMMIT_TAG}"
  script:
    - |
      {
        echo -n "release-cli create --name \"dependencies $CI_COMMIT_TAG\" --tag-name $CI_COMMIT_TAG "
        for ARTIFACT in jhb*.dmg; do
          echo -n "--assets-link '{\"name\":\"${ARTIFACT}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT}\"}' "
        done
      } > create_release.sh
      chmod 755 create_release.sh
      ./create_release.sh
