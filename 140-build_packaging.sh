#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install tools that are not direct dependencies but required for packaging.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

ABCREATE_VER="0.4.1"

### functions ##################################################################

# Nothing here.

### main #######################################################################

if $CI; then   # break in CI, otherwise we get interactive prompt by JHBuild
  error_trace_enable
fi

#-------------------------------------------- install application bundle creator

jhb run pip install abcreate==$ABCREATE_VER

#-------------------------------------------------- create disk image background

jhb build imagemagick

lib_change_path "$LIB_DIR"/libomp.dylib "$BIN_DIR"/convert
