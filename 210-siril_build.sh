#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Build Siril.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(greadlink -f "$(dirname "${BASH_SOURCE[0]}")")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#------------------------------------------------------- build and install Siril

# clone the repository if we're not running inside Siril CI
if [ "$CI_PROJECT_NAME" != "siril" ]; then
  git clone \
    "$SIRIL_URL" \
    "$SIRIL_SRC_DIR"
  git -C "$SIRIL_SRC_DIR" \
    checkout "$SIRIL_GIT_REF"
  git -C "$SIRIL_SRC_DIR" \
    submodule update --init --recursive
fi

# add custom build number
gsed -i "s/%s\\\nThis/%s (build $SIRIL_BUILD)\\\nThis/g" \
  "$SIRIL_SRC_DIR/src/gui/about_dialog.c"
gsed -i "s/%s\\\ncommit/%s (build $SIRIL_BUILD)\\\ncommit/g" \
  "$SIRIL_SRC_DIR/src/gui/about_dialog.c"

jhb run meson setup \
  --prefix "$VER_DIR" \
  --buildtype release \
  "$SIRIL_BLD_DIR" \
  "$SIRIL_SRC_DIR"

jhb run meson compile -C "$SIRIL_BLD_DIR"
jhb run meson install -C "$SIRIL_BLD_DIR"
