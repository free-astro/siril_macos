# SPDX-FileCopyrightText: 2024 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Set ccache version to 4.9.1 as 4.10 has introduced changes breaking
# Siril's OpenMP integration.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced

### variables ##################################################################

# shellcheck disable=SC2034 # no export necessary
CCACHE_VER=4.9.1

### functions ##################################################################

# Nothing here.

### main #######################################################################

# Nothing here.
