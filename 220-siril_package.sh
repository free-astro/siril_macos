#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the application bundle. This also includes patching library link
# paths and all other components that we need to make relocatable.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "$(greadlink -f "$0")")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------- create application bundle

cp "$SELF_DIR"/resources/siril_dev.icns "$TMP_DIR"/Siril.icns
cp "$SELF_DIR"/resources/Siril_files.icns "$TMP_DIR"
cp "$SELF_DIR"/resources/siril.plist "$TMP_DIR"

abcreate create "$SELF_DIR"/resources/applicationbundle.xml \
  -s "$VER_DIR" -t "$ART_DIR"

#------------------------------------------------------- configure fonts for GTK

# Updating Pango to >= 1.55 to fix a long-standing bug that makes everything
# completely unreadable (garbled fonts/characters) introduces a change in font
# kerning (or it's picking a different font now, idk), making everything harder
# to read and look at. So we manually configure GTK to use "SF Pro" for the UI.

{
  echo -e ""
  cat "$SELF_DIR"/resources/gtk.css
} >> "$SIRIL_APP_RES_DIR"/share/themes/Mac/gtk-3.0/gtk-keys.css

#-------------------------------------------------- add fontconfig configuration

# Our customized version loses all the non-macOS paths and sets a cache
# directory below "Application Support/Caches".
cp "$SELF_DIR"/resources/fonts.conf "$SIRIL_APP_ETC_DIR"/fonts

#---------------------------------------------------- add SSL certificate bundle

certifi_extract_cacert "$SIRIL_APP_ETC_DIR"/ca-certificates

#---------------------------------------------------------- add Python framework

siril_install_python "$TMP_DIR"

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$SIRIL_APP_PLIST"

# enable dark mode (menubar only, GTK theme is reponsible for the rest)
/usr/libexec/PlistBuddy -c "Add NSRequiresAquaSystemAppearance bool 'false'" \
  "$SIRIL_APP_PLIST"

# set minimum OS version according to deployment target
if [ -z "$MACOSX_DEPLOYMENT_TARGET" ]; then
  MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
fi
/usr/libexec/PlistBuddy \
  -c "Set LSMinimumSystemVersion $MACOSX_DEPLOYMENT_TARGET" \
  "$SIRIL_APP_PLIST"

# set Siril version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(siril_get_version_from_config_h)'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$SIRIL_BUILD'" \
  "$SIRIL_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c \
  "Add LSApplicationCategoryType string 'public.app-category.photography'" \
  "$SIRIL_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
'Siril needs your permission to access the Desktop folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
'Siril needs your permission to access the Documents folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
'Siril needs your permission to access the Downloads folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
'Siril needs your permission to access removeable volumes.'" \
  "$SIRIL_APP_PLIST"

# add some metadata to make CI identifiable
if $CI; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA COMMIT_SHORT_SHA\
             JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(\
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$SIRIL_APP_PLIST"
  done
fi
