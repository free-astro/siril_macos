# Siril for macOS

![siril](resources/siril.png)

The code from this repository produces the official macOS release for [Siril](https://siril.org). Due to its development history, it exists in two places:

| [dehesselle/siril_macos](https://gitlab.com/dehesselle/siril_macos) | [free-astro/siril_macos](https://gitlab.com/free-astro/siril_macos) |
| --- | --- |
| **test** | **production** |
| | used in Siril's CI |

Development began on the left side and it's still the place where test builds are created on occasion. The right side is for production usage, i.e. what Siril's CI uses to produce the app.

## download

Siril releases can be downloaded on the official [website](https://siril.org/download/). The app is standalone, relocatable and supports macOS Big Sur up to macOS Sequoia.

Development snapshots are available directly from Siril's CI, look for `macos` jobs in the `packaging` stage of a [pipeline](https://gitlab.com/free-astro/siril/-/pipelines) you are interested in.

## data locations

Siril uses the following locations to store data on macOS:

| type | path |
| --- | --- |
| configuration files | `~/Library/Application Support/org.siril.Siril` |
| scripts | `~/Library/Application Support/org.siril.Siril/scripts` <br> `~/.siril/scripts` <br>  `~/siril/scripts`|
| cache files | `~/Library/Caches/org.siril.Siril` <br> `~/Library/Caches/com.gitlab.dehesselle.python_macos` |

## license

This work is licensed under [GPL-2.0-or-later](LICENSE).  
Siril is licensed under [GPL-3.0-or-later](https://gitlab.com/free-astro/siril/-/blob/master/LICENSE.md).

### additional credits

Built using other people's work:

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) licensed under GPL-2.0-or-later.
